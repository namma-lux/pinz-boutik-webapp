import * as respVerifier from './response-verifier';

const URL_API_HOSTNAME           = window.location.hostname;
const URL_API_RETRIEVE_JWT_TOKEN = `https://${URL_API_HOSTNAME}:8443/auth/token`;

export async function login (email, pwd) {
  const jwtToken = await retrieveJwtToken(email, pwd);

  const userProfileBase64 = jwtToken.split('.')[1];
  const userProfileJson   = Buffer.from(userProfileBase64, 'base64').toString();
  const userProfile       = JSON.parse(userProfileJson);

  return { jwtToken, userProfile };
}


async function retrieveJwtToken (email, password) {
  const data = { email, password };
  const resp = await fetch(URL_API_RETRIEVE_JWT_TOKEN, {
    method : 'POST',
    headers: { 'Content-Type': 'application/json' },
    body   : JSON.stringify(data),
  });

  respVerifier.verifyApiResponse(resp);

  const payload = await resp.json();
  return payload.data.jwt_token;
}