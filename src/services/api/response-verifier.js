/**
 * @param {Response} resp
 */
export function verifyApiResponse (resp) {
  const { status } = resp;

  if (status < 400) return;

  if (status === 400) throw new Api400Error(resp, resp.statusText);
  if (status === 401) throw new Api401Error(resp, resp.statusText);
  if (status === 403) throw new Api403Error(resp, resp.statusText);
  if (status === 404) throw new Api404Error(resp, resp.statusText);

  throw new ApiError(resp, resp.statusText);
};

export class ApiError extends Error {
  constructor (resp, msg) {
    super(msg);
    this.name = this.constructor.name;
    this.resp = resp;

    Error.captureStackTrace(this, this.constructor);
  }
};

export class Api400Error extends ApiError {};

export class Api401Error extends ApiError {};

export class Api403Error extends ApiError {};

export class Api404Error extends ApiError {};