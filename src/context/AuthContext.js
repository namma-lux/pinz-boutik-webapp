import Cookies from 'js-cookie';
import React, { useEffect, useReducer } from 'react';
import { useHistory } from 'react-router-dom';

const authInitialState = {
  jwtToken   : Cookies.get('jwt_token'),
  userProfile: null,
};

const authReducer = (state = authInitialState, action) => {
  const { payload } = action;
  switch (action.type) {
    case 'AUTH_OK':
      return {
        ...state,
        jwtToken   : payload.jwtToken,
        userProfile: payload.userProfile,
      };

    case 'AUTH_FAILED':
      return {
        ...state,
        jwtToken   : null,
        userProfile: null,
      };

    default:
      return state;
  }
};

const AuthContext = React.createContext(authInitialState);
export default AuthContext;

export const AuthContextWrapper = (props) => {
  const history = useHistory();
  const [authState, dispatchAuthState] = useReducer(authReducer, authInitialState);

  const emitTokenRetrieved = (jwtToken, userProfile) => {
    dispatchAuthState({
      type: 'AUTH_OK',
      payload: { jwtToken, userProfile },
    });
  };

  const emitAuthFailed = () => {
    dispatchAuthState({
      type: 'AUTH_FAILED'
    });
  };

  useEffect(() => {
    if (! authState.jwtToken) {
      Cookies.remove('jwt_token');
      return history.push('/login');
    }
  }, [history, authState.jwtToken]);

  return (
    <AuthContext.Provider value={{
      jwtToken   : authState.jwtToken,
      userProfile: authState.userProfile,
      emitTokenRetrieved,
      emitAuthFailed,
    }}>
      { props.children }
    </AuthContext.Provider>
  );
};

export const AuthContextConsumer = AuthContext.Consumer;
