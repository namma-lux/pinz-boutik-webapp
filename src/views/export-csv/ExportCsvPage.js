import { CButton, CCard, CCardHeader, CFormGroup, CInput, CLabel, CSelect } from '@coreui/react';
import React, { useCallback, useContext, useEffect, useReducer, useState } from 'react';
import AuthContext from 'src/context/AuthContext';

const URL_API_HOSTNAME              = window.location.hostname;
const URL_API_GET_WEBSITES          = `https://${URL_API_HOSTNAME}:8443/websites`;
const URL_API_GET_BOOK_CATEGORIES   = `https://${URL_API_HOSTNAME}:8443/book-categories`;
const URL_API_GET_RATING_TYPES      = `https://${URL_API_HOSTNAME}:8443/book-rating-types`;
const URL_API_GET_EXPORT_HISTORIES  = `https://${URL_API_HOSTNAME}:8443/websites/:id/export-histories`;
const URL_API_EXPORT_CSV            = `https://${URL_API_HOSTNAME}:8443/export-csv`;
const URL_API_DOWNLOAD_EXPORTED_CSV = `https://${URL_API_HOSTNAME}:8443/export-csv/download`;

const formDataInitialState = {
  website_id:       '',
  book_categories:  [],
  rating_price_map: {},
};

const formDataReducer = (state, action) => {
  const { payload } = action;
  switch (action.type) {
    case 'SET_WEBSITE_ID':
      return { ...state, website_id: payload };
    case 'SET_BOOK_CATEGORIES':
      return { ...state, book_categories: payload };
    case 'SET_PRICE_INIT_FOR_RATING_TYPE': {
      const { ratingType, priceMin: price_min, priceMax: price_max } = payload;
      const rating_price_map = { ...state.rating_price_map };

      rating_price_map[ratingType] = { price_min, price_max };
      return { ...state, rating_price_map };
    }
    case 'SET_PRICE_MIN_FOR_RATING_TYPE': {
      const { ratingType, priceMin } = payload;
      const rating_price_map = { ...state.rating_price_map };

      if (! rating_price_map[ratingType])
        rating_price_map[ratingType] = {};

      rating_price_map[ratingType].price_min = priceMin;
      return { ...state, rating_price_map };
    }
    case 'SET_PRICE_MAX_FOR_RATING_TYPE': {
      const { ratingType, priceMax } = payload;
      const rating_price_map = { ...state.rating_price_map };

      if (! rating_price_map[ratingType])
        rating_price_map[ratingType] = {};

      rating_price_map[ratingType].price_max = priceMax;
      return { ...state, rating_price_map };
    }
    default:
      throw new Error(`Action type [${action.type}] not supported.`);
  }
};

const ExportCsvPage = () => {
  const authContext                      = useContext(AuthContext);
  const [websites, setWebsites]          = useState([]);
  const [categories, setCategories]      = useState([]);
  const [ratingTypes, setRatingTypes]    = useState([]);
  const [isInitiated, setInitiatedState] = useState(false);

  const [exportHistories, setExportHistories] = useState([]);

  const [formData, dispatchFormData]       = useReducer(formDataReducer, formDataInitialState);
  const [isSubmitting, setSubmittingState] = useState(false);
  const [respErrMsgs, setRespErrMsgs]      = useState([]);

  const reloadExportHistories = useCallback(() => {
    requestApiGetExportHistories(authContext.jwtToken, formData.website_id)
      .then(async (resp) => {
        const payload = await resp.json();
        setExportHistories(payload.data);
      });
  }, [authContext.jwtToken, formData.website_id]);

  useEffect(() => {
    const isInitiated = !! (websites.length && categories.length && ratingTypes.length);
    setInitiatedState(isInitiated);
  }, [websites, categories, ratingTypes]);

  useEffect(() => {
    requestApiGetWebsites(authContext.jwtToken)
      .then(async (resp) => {
        const payload = await resp.json();
        setWebsites(payload.data);
      });
    requestApiGetBookCategories(authContext.jwtToken)
      .then(async (resp) => {
        const payload = await resp.json();
        setCategories(payload.data);
      });
    requestApiGetRatingTypes(authContext.jwtToken)
      .then(async (resp) => {
        const payload = await resp.json();
        const ratingTypes = payload.data;

        ratingTypes.forEach(ratingType => dispatchFormData({
          type: 'SET_PRICE_INIT_FOR_RATING_TYPE',
          payload: {
            ratingType: ratingType.value,
            priceMin:   ratingType.defaultPriceMin,
            priceMax:   ratingType.defaultPriceMax,
          }
        }));

        setRatingTypes(ratingTypes);
      });
  }, []);

  useEffect(() => {
    // Clear before request.
    setExportHistories([]);

    if (formData.website_id)
      reloadExportHistories();
  }, [formData.website_id, reloadExportHistories]);

  function dispatchFormDataFromInputEvent (type, evt, ...args) {
    let payload = undefined;

    switch (type) {
      case 'SET_BOOK_CATEGORIES': {
        const selectedOptElmts = [...evt.currentTarget.children].filter(optElmt => optElmt.selected);
        payload = selectedOptElmts.map(optElmt => optElmt.value);
        break;
      }
      case 'SET_PRICE_MIN_FOR_RATING_TYPE': {
        const [ratingType] = args;
        const priceMin     = evt.currentTarget.value;
        payload = { ratingType, priceMin };
        break;
      }
      case 'SET_PRICE_MAX_FOR_RATING_TYPE': {
        const [ratingType] = args;
        const priceMax     = evt.currentTarget.value;
        payload = { ratingType, priceMax };
        break;
      }
      default:
        payload = evt.currentTarget.value;
    }
    dispatchFormData({ type, payload });
  }

  async function onFormSubmit (evt) {
    evt && evt.preventDefault();

    if (isSubmitting) return;

    setSubmittingState(true);
    try {
      const resp = await requestApiExportCsv(authContext.jwtToken, formData);
      if (resp.status < 400) {
        const payload         = await resp.json();
        const { csvFilename } = payload;
        const downloadUrl     = `${URL_API_DOWNLOAD_EXPORTED_CSV}/${csvFilename}`;

        window.open(downloadUrl, '_blank');
        reloadExportHistories();

      } else {
        const payload = await resp.json();
        if (resp.status === 400 && payload.errorMessages) {
          setRespErrMsgs(filterErrorMessages(payload.errorMessages));
          window.scrollTo(0, 0);
        } else if (resp.status === 401) {
          authContext.emitAuthFailed();
        } else {
          throw new Error(payload.message || resp.statusText || 'Internal server error.');
        }
      }
    } catch (err) {
      console.error(err);
      alert(err.message);

    } finally {
      setSubmittingState(false);
    }
  }

  if (! isInitiated)
    return (
      <div className="content">
        Loading...
      </div>
    );

  return (
    <div className="content">
      <div className="row">
        <div className="col-sm-8 col-md-7 col-lg-6">
          <form onSubmit={onFormSubmit}>
            <div className="card overflow-hidden">
              <div className="card-header">
                <h3 className="card-title m-0">
                  <span className="mr-3">Export CSV</span>
                </h3>
              </div>
              <div className="card-body">
                { (respErrMsgs.length > 0) &&
                  <CCard color='danger'>
                    <CCardHeader>
                      { respErrMsgs.map(
                        (value, index) => <div key={index} style={{ color: 'white' }}>{value}</div>
                      ) }
                    </CCardHeader>
                  </CCard>
                }

                <CFormGroup>
                  <CLabel>Websites</CLabel>
                  <CSelect
                    value    = {formData.website_id}
                    onChange = {evt => dispatchFormDataFromInputEvent('SET_WEBSITE_ID', evt)}
                  >
                    <option key={null} value=""></option>
                    {websites.map((site) => <option key={site._id} value={site._id}>{site.name}</option>)}
                  </CSelect>
                </CFormGroup>

                <CFormGroup>
                  <CLabel>Category</CLabel>
                  <CSelect multiple
                    value    = {formData.book_categories}
                    onChange = {evt => dispatchFormDataFromInputEvent('SET_BOOK_CATEGORIES', evt)}
                    style    = {{ height: '320px' }}
                  >
                    {categories.map((cate) => <option key={cate} value={cate}>{cate}</option>)}
                  </CSelect>
                </CFormGroup>
              </div>
              { ratingTypes.map(ratingType => (
                <div key={ratingType.value} className="card-body border-top">
                  <h5>Pricing: {ratingType.label}</h5>
                  <CFormGroup>
                    <CLabel>Min Price</CLabel>
                    <CInput
                      value    = {formData.rating_price_map[ratingType.value].price_min}
                      onChange = {evt => dispatchFormDataFromInputEvent('SET_PRICE_MIN_FOR_RATING_TYPE', evt, ratingType.value)}
                    />
                  </CFormGroup>

                  <CFormGroup>
                    <CLabel>Max Price</CLabel>
                    <CInput
                      value    = {formData.rating_price_map[ratingType.value].price_max}
                      onChange = {evt => dispatchFormDataFromInputEvent('SET_PRICE_MAX_FOR_RATING_TYPE', evt, ratingType.value)}
                    />
                  </CFormGroup>
                </div>
              )) }
              <div className="card-body">
                <CButton type="submit" size="sm" color="primary" disabled={isSubmitting}>{isSubmitting ? 'Exporting...' : 'Export'}</CButton>
              </div>
            </div>
          </form>
        </div>

        { (exportHistories.length > 0) &&
          <div className="col-sm-4 col-md-5 col-lg-6">
            <div className="card overflow-hidden">
              <div className="card-header">
                <h3 className="card-title m-0">
                  <span className="mr-3">Export History Logs</span>
                </h3>
              </div>
                <table className="table table-sm m-0">
                  <thead>
                    <tr>
                      <th>Book Category</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    { exportHistories.map(historyRecord => (
                        <tr key={historyRecord._id}>
                          <td>{historyRecord.book_category}</td>
                          <td>{historyRecord.date}</td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
            </div>
          </div>
        }
      </div>
    </div>
  );
};
export default ExportCsvPage;


async function requestApiGetWebsites (token) {
  return await fetch(URL_API_GET_WEBSITES, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

async function requestApiGetBookCategories (token) {
  return await fetch(URL_API_GET_BOOK_CATEGORIES, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

async function requestApiGetRatingTypes (token) {
  return await fetch(URL_API_GET_RATING_TYPES, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

async function requestApiGetExportHistories (token, websiteId) {
  const url = URL_API_GET_EXPORT_HISTORIES.replace(':id', websiteId);
  return await fetch(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

async function requestApiExportCsv (token, formData) {
  return await fetch(URL_API_EXPORT_CSV, {
    method: 'POST',
    headers: {
      Authorization:  `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formData),
  });
}

function filterErrorMessages (respErrMsgsObj) {
  return Object.values(respErrMsgsObj)
    .map(errMsgs => errMsgs[0]);
}