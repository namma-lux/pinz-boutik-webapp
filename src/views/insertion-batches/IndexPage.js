import { CButton } from "@coreui/react";
import React, { useCallback, useContext, useEffect, useMemo, useState } from "react";
import ReactDatatable from "@ashvin27/react-datatable";
import { Link } from "react-router-dom";
import moment from "moment";
import AuthContext from "src/context/AuthContext";


const URL_API_HOSTNAME         = window.location.hostname;
const URL_API_GET_BATCHES_LIST = `https://${URL_API_HOSTNAME}:8443/insertion-batches`;
const URL_API_POST_BATCH_RETRY = `https://${URL_API_HOSTNAME}:8443/insertion-batches/{batchId}/retry`;

const BatchIndexPage = () => {
  const authContext = useContext(AuthContext);

  const [batches, setBatches] = useState([]);

  const callApiGetBatchesList = useCallback(async () => {
    const token = authContext.jwtToken;

    const resp = await fetch(URL_API_GET_BATCHES_LIST, {
      headers: { Authorization: `Bearer ${token}` }
    });

    if (resp.status === 401)
      return authContext.emitAuthFailed();

    const payload = await resp.json();
    setBatches(payload.data);
  }, [authContext]);

  const onClickBtnRetry = useCallback(async (evt) => {
    const btn   = evt.currentTarget;
    const token = authContext.jwtToken;

    const batchId = btn.dataset.batchId;
    const url     = URL_API_POST_BATCH_RETRY.replace('{batchId}', batchId);

    function disableButton () {
      btn.disabled  = true;
      btn.innerText = 'Pushing jobs...';
    }

    function enableButton () {
      btn.disabled  = false;
      btn.innerText = 'Retry';
    }

    disableButton();
    try {
      const resp = await fetch(url, {
        method : 'POST',
        headers: { Authorization: `Bearer ${token}` }
      });

      if (resp.status === 401)
        return authContext.emitAuthFailed();

    } finally {
      enableButton();
    }

  }, [authContext]);

  const columns = useMemo(() => [
    {
      key: 'name',
      text: 'Name',
      width: 200,
      cell: record => <span>{record.name}</span>,
    },
    {
      key: 'completed_by_total',
      text: 'Completed/Total',
      className: 'text-right',
      align: 'right',
      width: 100,
      cell: record => <span>{record.books_count_completed}/{record.books_count_total}</span>,
    },
    {
      key: 'inserted_at',
      text: 'Inserted At',
      width: 100,
      cell: record => <span>{moment(record.inserted_at).format('YYYY-MM-DD')}</span>,
    },
    {
      key: 'updated_at',
      text: 'Updated At',
      width: 100,
      cell: record => <span>{moment(record.updated_at).format('YYYY-MM-DD')}</span>,
    },
    {
      key: 'retry',
      text: '',
      className: 'text-right',
      cell: record => (
        <button
          className="btn btn-outline-warning btn-sm"
          onClick={onClickBtnRetry}
          data-batch-id={record._id}
        >Retry</button>
      ),
    }
  ], [onClickBtnRetry]);

  useEffect(() => {
    callApiGetBatchesList();
  }, []);

  return (
    <div className="content">
      <div className="card overflow-hidden">
        <div className="card-header">
          <h3 className="card-title m-0">
            <span className="mr-3">Insertion Batches</span>
            <Link to="/insertion-batches/new">
              <CButton variant="outline" color="info" size="sm">New Batch</CButton>
            </Link>
          </h3>
        </div>
        <div className="table-responsive">
          <ReactDatatable
            dynamic
            columns={columns}
            records={batches}
            config={{
              key_column      : '_id',
              show_filter     : false,
              show_pagination : false,
              show_length_menu: false,
              show_info       : false,
              sort: {
                column: 'history.inserted_at',
                order : 'desc',
              },
            }}
            // onChange={onTableActionHandler}
            className="table table-striped m-0 mt-n3 border-bottom"
          />
        </div>
      </div>
    </div>
  );
};
export default BatchIndexPage;