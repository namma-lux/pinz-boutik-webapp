import { CButton, CFormGroup, CInput, CLabel, CTextarea } from "@coreui/react";
import React, { useCallback, useContext, useReducer, useState } from "react";
import AuthContext from "src/context/AuthContext";

const URL_API_HOSTNAME      = window.location.hostname;
const URL_API_IMPORT_LIBGEN = `http://${URL_API_HOSTNAME}:8443/insertion-batches/libgen`;

const formDataInitialState = {
  insertion_batch_name: '',
  str_libgen_list:      '',
};

const formDataReducer = (state, action) => {
  const { payload } = action;
  switch (action.type) {
    case 'SET_BATCH_NAME':
      return { ...state, insertion_batch_name: payload };

    case 'SET_STR_LIBGEN_LIST':
      return { ...state, str_libgen_list: payload };

    default:
      throw new Error(`Action type [${action.type}] not supported.`);
  }
};

const BatchNewPage = () => {
  const authContext = useContext(AuthContext);

  const [formData, dispatchFormData] = useReducer(formDataReducer, formDataInitialState);
  const setBatchName     = value => dispatchFormData({ type: 'SET_BATCH_NAME',      payload: value });
  const setStrIsbn13List = value => dispatchFormData({ type: 'SET_STR_LIBGEN_LIST', payload: value });

  const [isSubmitting, setSubmittingState]                = useState(false);
  const [formValidationDetails, setFormValidationDetails] = useState(null);
  const [result,setResult]                                = useState(null);
  const clearFormValidationDetails = () => setFormValidationDetails(null);
  const clearResult                = () => setResult(null);

  const onIsbn13FormSubmit = useCallback(
    async (evt) => {
      evt.preventDefault();

      clearFormValidationDetails();
      clearResult();
      setSubmittingState(true);

      const body = JSON.stringify(formData);

      try {
        const token = authContext.jwtToken;
        const resp = await fetch(URL_API_IMPORT_LIBGEN, {
          method : 'POST',
          headers: {
            Authorization : `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
          body,
        });
        const respOk  = resp.status < 400;
        const payload = await resp.json();

        if (respOk) {
          return setResult(payload);
        } else {
          if ((resp.status === 400) && (payload.name === 'ValidationError'))
            return setFormValidationDetails(payload.errorMessages);
          if (resp.status === 401)
            return authContext.emitAuthFailed();

          throw new Error(payload.message || resp.statusText || 'Internal server error.');
        }
      } catch (err) {
        console.error(err);
        alert(err.message);

      } finally {
        setSubmittingState(false);
      }
    },
    [formData, authContext]
  );

  return (
    <div className="content">
      <div className="row">
        <div className="col">
          <div className="card overflow-hidden">
            <div className="card-body bg-light">
              <ImportLibgenForm
                onBatchNameChange={ setBatchName }
                onStrIsbn13ListChange={ setStrIsbn13List }
                onSubmit={ onIsbn13FormSubmit }
                isSubmitting={ isSubmitting }
              />
            </div>{/* .card-body */}
          </div>{/* .card */}
        </div>
        <div className="col">
          { formValidationDetails &&
            <div className="card">
              <div className="card-body bg-warning">
                <pre>
                  <code>{ JSON.stringify(formValidationDetails, null, 2) }</code>
                </pre>
              </div>
            </div>
          }
          { result &&
            <div>
              <div className="card overflow-hidden text-white">
                <div className="card-body bg-success" id="block-inserted-count">
                  <h6>Successfully imported <u>{result.insertedCount}</u> record(s).</h6>
                </div>
              </div>
              <div className="card overflow-hidden">
                <div className="card-body bg-warning" id="block-insertions-skipped">
                  <h6>Skipped</h6>
                  <pre>
                    <code>{ JSON.stringify(result.insertionsSkipped, null, 2) }</code>
                  </pre>
                </div>
              </div>
              <div className="card overflow-hidden">
                  <div className="card-body bg-danger text-white" id="block-insertions-failed">
                    <h6>Errors</h6>
                    <pre className="text-white">
                      <code>{ JSON.stringify(result.insertionsFailed, null, 2) }</code>
                    </pre>
                  </div>
                </div>
            </div>
          }
        </div>
      </div>
    </div>
  );
};
export default BatchNewPage;

const ImportLibgenForm = (props) => {
  return (
    <form onSubmit={ props.onSubmit }>
      <CFormGroup>
        <CLabel htmlFor="txt-batch-name">Batch Name</CLabel>
        <CInput name="insertion_batch_name" id="txt-batch-name"
          onChange={ evt => props.onBatchNameChange(evt.currentTarget.value) }
        />
      </CFormGroup>
      <CFormGroup>
        <CLabel htmlFor="txt-str-isbn13-list">List of Libgen Links (line by line)</CLabel>
        <CTextarea name="str_libgen_list" id="txt-str-isbn13-list"
          onChange={ evt => props.onStrIsbn13ListChange(evt.currentTarget.value) }
          style={{
            resize   : 'vertical',
            height   : 256,
            width    : 728,
            maxHeight: 'none',
          }}
        />
      </CFormGroup>
      <div className="text-center">
        <CButton disabled={props.isSubmitting} type="submit" color="dark" size="sm" variant="outline">
          { props.isSubmitting ? 'Importing...' : 'Import' }
        </CButton>
      </div>
    </form>
  );
};
