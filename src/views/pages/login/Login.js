import React, { useCallback, useContext, useEffect, useReducer } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';
import * as authService from '../../../services/api/auth';
import AuthContext from '../../../context/AuthContext';

const authFormInitialState = {
  email   : '',
  password: '',
};

const authFormReducer = (state, action) => {
  const { payload } = action;
  switch (action.type) {
    case 'SET_EMAIL':
      return { ...state, email: payload };
    case 'SET_PASSWORD':
      return { ...state, password: payload };
    default:
      throw new Error(`Action type [${action.type}] not supported`);
  }
}

const Login = () => {
  const history     = useHistory();
  const authContext = useContext(AuthContext);

  const [authFormState, dispatchAuthFormState] = useReducer(authFormReducer, authFormInitialState);

  function setEmail (email) {
    dispatchAuthFormState({ type: 'SET_EMAIL', payload: email });
  }

  function setPassword (password) {
    dispatchAuthFormState({ type: 'SET_PASSWORD', payload: password });
  }

  const onAuthFormSubmitHandler = useCallback(evt => {
    evt.preventDefault();

    const onSuccess = ({ jwtToken, userProfile }) => {
      Cookies.set('jwt_token', jwtToken);
      authContext.emitTokenRetrieved(jwtToken, userProfile);
    };

    const onError = err => alert(err.message);

    authService
      .login(authFormState.email, authFormState.password)
      .then(onSuccess)
      .catch(onError);
  }, [authContext, authFormState]);

  useEffect(() => {
    if (authContext.jwtToken) {
      history.push('/books');
    }
  }, [history, authContext.jwtToken]);

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="5">
            <CCard className="p-4">
              <CCardBody>
                <CForm onSubmit={onAuthFormSubmitHandler}>
                  <h1>Authentication</h1>
                  <p className="text-muted">Sign In to your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-user" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="text" placeholder="Email" autoComplete="email"
                      value={authFormState.email}
                      onChange={evt => setEmail(evt.currentTarget.value)}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="password" placeholder="Password" autoComplete="current-password"
                      value={authFormState.password}
                      onChange={evt => setPassword(evt.currentTarget.value)}
                    />
                  </CInputGroup>
                  <CRow>
                    <CCol xs="6">
                      <CButton type="submit" color="primary" className="px-4">Sign in</CButton>
                    </CCol>
                  </CRow>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
