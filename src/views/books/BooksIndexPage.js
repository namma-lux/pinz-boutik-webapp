import ReactDatatable from '@ashvin27/react-datatable';
import { CButton, CButtonGroup, CFormGroup, CInput, CInputGroup, CInputGroupPrepend, CLabel, CLink, CSelect } from '@coreui/react';
import React, { Fragment, useCallback, useContext, useEffect, useReducer, useState } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import AuthContext from '../../context/AuthContext';
import CIcon from '@coreui/icons-react';

const URL_API_HOSTNAME       = window.location.hostname;
const URL_API_GET_BOOKS_LIST = `https://${URL_API_HOSTNAME}:8443/books`;

const PER_PAGE = 50;

const columns = [
  {
    key     : 'image',
    text    : 'Image',
    width   : 200,
    cell: record => {
      const { source_url, b2_url } = record.image;

      if (b2_url)
        return (
          <div className="text-right">
            <img src={b2_url} alt="Failed to show." height={100} style={{ boxShadow: '0 0 2px 1px lightgrey' }} />
          </div>
        );
      else if (source_url)
        return <span>Not yet collected.<br/><a href={source_url} target="_blank" rel="noopener noreferrer">Source link</a></span>
    },
  },
  {
    key     : 'title',
    text    : 'Title',
    sortable: true,
    cell: record => {
      const isImageCollected = !! record.file.b2_url;
      const isFileCollected  = !! record.file.b2_url;
      const isCompleted      = (isImageCollected && isFileCollected);

      return (
        <Fragment>
          {isCompleted && <CIcon name="cilCheckCircle" size="sm" className="text-success mr-2" />}
          <Link to={`/books/${record._id}`} target="_blank">
            {record.title || '[Untitled]'}
          </Link>
          { record.file.b2_url &&
            <div className="mt-2">
              <CButton color="primary" size="sm" href={record.file.b2_url} target="_blank">Download</CButton>
            </div>
          }
        </Fragment>
      )
    },
  },
  {
    key            : 'metadata.edition',
    text           : 'Edition',
    sortable       : true,
    cell: record => {
      return record.metadata.edition;
    },
  },
  {
    key            : 'metadata.rating',
    text           : 'Rating',
    sortable       : true,
    TrOnlyClassName: 'text-right pr-4',
    className      : 'text-right pr-4',
    cell: record => {
      return record.metadata.rating;
    },
  },
  {
    key      : 'file.ext',
    text     : 'Ext.',
    width    : 80,
    align    : 'right',
    className: 'text-right',
    cell: record => record.file.ext,
  },
  {
    key      : 'type',
    text     : 'Type',
    width    : 80,
    sortable : true,
  },
  {
    key     : 'isbn_list',
    text    : 'ISBN',
    width   : 150,
    cell: record => {
      const isbn10   = ('book' === record.type) ? record.isbn10: record.isbn10_related;
      const isbn13   = ('book' === record.type) ? record.isbn13: record.isbn13_related;
      const isbnList = [];
      if (isbn10)
        isbnList.push(isbn10);
      if (isbn13)
        isbnList.push(isbn13);

      return isbnList.map(isbn => <div key={isbn}>{isbn}</div>);
    },
  },
  {
    key     : 'metadata.category',
    text    : 'Category',
    width   : 150,
    sortable: true,
    cell: record => record.metadata.category,
  },
  {
    key     : 'history.inserted_at',
    text    : 'Inserted',
    width   : 150,
    sortable: true,
    cell: record => {
      const insertedAt = record.history.inserted_at;
      const employee   = record.history.inserted_by;
      const link       = employee && <span>by <Link to = {`/users/${employee._id}`} target="_blank">{employee.name}</Link></span>;

      return <Fragment>{moment(insertedAt).format('DD/MM/YYYY')}<br/>{link}</Fragment>;
    },
  },
];

const formDataInitialState = {
  search_by        : 'isbn',
  keyword          : '',
  completion_status: '',
  type             : '',
};

const formDataReducer = (state, action) => {
  const { payload } = action;

  switch (action.type) {
    case 'SET':
      return { ...state, ...action.payload };

    case 'SET_SEARCH_BY':
      return { ...state, search_by: payload };
    case 'SET_KEYWORD':
      return { ...state, keyword: payload };
    case 'SET_COMPLETION_STATUS':
      return { ...state, completion_status: payload };
    case 'SET_TYPE':
      return { ...state, type: payload };

    default:
      throw new Error(`Action type [${action.type}] not supported.`);
  }
};

const BooksIndexPage = () => {
  const authContext = useContext(AuthContext);

  const [books,      setBooks]        = useState([]);
  const [booksTotal, setBooksTotal]   = useState(0);
  const [maxPage,    setMaxPage]      = useState(1);
  const [page,       setPage]         = useState(1);
  const [sort,       setSort]         = useState({ column: 'history.inserted_at', order: 'desc' });

  const [formData, dispatchFormData] = useReducer(formDataReducer, formDataInitialState);
  const setSearchBy         = useCallback(payload => dispatchFormData({ payload, type: 'SET_SEARCH_BY'}), []);
  const setKeyword          = useCallback(payload => dispatchFormData({ payload, type: 'SET_KEYWORD'}), []);
  const setCompletionStatus = useCallback(payload => dispatchFormData({ payload, type: 'SET_COMPLETION_STATUS'}), []);
  const setBookType         = useCallback(payload => dispatchFormData({ payload, type: 'SET_TYPE' }), []);

  const onTableActionHandler = useCallback(action => setSort(action.sort_order), []);

  const callApiGetBooksList = useCallback(async (page = 1, sort) => {
    const { search_by, keyword, completion_status, type } = formData;
    const token = authContext.jwtToken;

    const url = new URL(URL_API_GET_BOOKS_LIST);
    url.searchParams.append('page_index', page - 1);
    url.searchParams.append('page_size', PER_PAGE);
    url.searchParams.append('sort[column]', sort.column);
    url.searchParams.append('sort[order]', (sort.order === 'asc') ? 1 : -1);
    if (keyword) {
      url.searchParams.append('filter[search_by]', search_by);
      url.searchParams.append('filter[keyword]', keyword);
    }
    if (completion_status) {
      url.searchParams.append('filter[completion_status]', completion_status);
    }
    if (type) {
      url.searchParams.append('filter[type]', type);
    }

    const resp = await fetch(url, {
      headers: { Authorization: `Bearer ${token}` }
    });

    if (resp.status === 401)
      return authContext.emitAuthFailed();

    const payload = await resp.json();
    setBooks(payload.data);
    setBooksTotal(payload.total);
    setMaxPage(Math.ceil(payload.total / PER_PAGE));

  }, [authContext, formData]);

  const increasePage = useCallback(() => (page < maxPage) && setPage(page + 1), [page, maxPage]);
  const decreasePage = useCallback(() => (page > 1)       && setPage(page - 1), [page]);

  const onSearchFormSubmit = useCallback(evt => {
    evt && evt.preventDefault();

    if (page !== 1)
      setPage(1);
    else
      callApiGetBooksList(1, sort);

  }, [callApiGetBooksList, page, sort]);

  // This component's initialize callback (run once);
  useEffect(() => {
    callApiGetBooksList(page, sort);
  }, [page, sort]);

  return (
    <div className="content">
      <div className="card overflow-hidden">
        <div className="card-header">
          <h3 className="card-title m-0">
            <span className="mr-3">Books</span>
            <Link to="/books/new">
              <CButton variant="outline" color="info" size="sm">New Book</CButton>
            </Link>
          </h3>
        </div>
        <div className="card-body pb-0">
          <form onSubmit={onSearchFormSubmit}>
            <div className="row">
              <div className="col-xl-4">
                <CFormGroup>
                  <CLabel>Keyword</CLabel>
                  <CInputGroup size="sm">
                    <CInputGroupPrepend>
                      <CSelect name="search_by" size="sm" className="border-info"
                        value={formData.search_by}
                        onChange={evt => setSearchBy(evt.currentTarget.value)}
                      >
                        <option value="isbn">ISBN</option>
                        <option value="inserted_by">Added by (email)</option>
                        <option value="category">Category</option>
                        <option value="title">Title</option>
                      </CSelect>
                    </CInputGroupPrepend>
                    <CInput type="text" name="keyword" placeholder="Keyword" className="border-info"
                      value={formData.keyword}
                      onChange={evt => setKeyword(evt.currentTarget.value)}
                    />
                  </CInputGroup>
                </CFormGroup>
              </div>
              <div className="col-xl-3">
                <CFormGroup>
                  <CLabel>Completion Status</CLabel>
                  <CSelect size="sm" className="border-info"
                    value={formData.completion_status}
                    onChange={evt => setCompletionStatus(evt.currentTarget.value)}
                  >
                    <option value="">--- All ---</option>
                    <option value="incomplete">Incomplete</option>
                    <option value="image_not_collected">Image not collected</option>
                    <option value="file_not_collected">Book-file not collected</option>
                    <option value="image_collected_not_file">Image collected w/o book-file</option>
                    <option value="file_collected_not_image">Book-file collected w/o image</option>
                    <option value="completed">Completed</option>
                  </CSelect>
                </CFormGroup>
              </div>
              <div className="col-xl-2">
                <CFormGroup>
                  <CLabel>Book Type</CLabel>
                  <CSelect size="sm" className="border-info"
                    value={formData.type}
                    onChange={evt => setBookType(evt.currentTarget.value)}
                  >
                    <option value="">--- All ---</option>
                    <option value="book">Standard book</option>
                    <option value="solution-manual">Solution manual</option>
                    <option value="test-bank">Test bank</option>
                  </CSelect>
                </CFormGroup>
              </div>
              <div className="col">
                <CLabel>&nbsp;</CLabel><br />
                <CButton type="submit" color="info" size="sm">Search</CButton>
              </div>
            </div>
          </form>
        </div>
        <div className="card-body font-xs py-2">
          <div className="row">
            <div className="col">
              Found {booksTotal} record(s).
            </div>
            <div className="col text-right">
              Page <CInput value={page} onChange={evt => setPage(parseInt(evt.currentTarget.value) || 1)} size="sm" className="d-inline text-right" style={{ width: '4em', height: 20 }} /> / {maxPage}
              <CButtonGroup size="sm" className="ml-2">
                <CButton variant="outline" color="secondary" className="text-dark font-xs py-0" onClick={decreasePage}>&lt; Prev</CButton>
                <CButton variant="outline" color="secondary" className="text-dark font-xs py-0" onClick={increasePage}>Next &gt;</CButton>
              </CButtonGroup>
            </div>
          </div>
        </div>
        <div className="table-responsive">
          <ReactDatatable
            dynamic
            columns={columns}
            records={books}
            config={{
              key_column      : '_id',
              show_filter     : false,
              show_pagination : false,
              show_length_menu: false,
              show_info       : false,
              sort: {
                column: 'history.inserted_at',
                order : 'desc',
              },
            }}
            onChange={onTableActionHandler}
            className="table table-striped m-0 mt-n3 border-bottom"
          />
        </div>
        <div className="card-body font-xs py-2">
          <div className="row">
            <div className="col">
              Found {booksTotal} record(s).
            </div>
            <div className="col text-right">
              Page <CInput value={page} onChange={evt => setPage(parseInt(evt.currentTarget.value) || 1)} size="sm" className="d-inline text-right" style={{ width: '4em', height: 20 }} /> / {maxPage}
              <CButtonGroup size="sm" className="ml-2">
                <CButton variant="outline" color="secondary" className="text-dark font-xs py-0" onClick={decreasePage}>&lt; Prev</CButton>
                <CButton variant="outline" color="secondary" className="text-dark font-xs py-0" onClick={increasePage}>Next &gt;</CButton>
              </CButtonGroup>
            </div>
          </div>
        </div>
      </div>
      <div className="my-4 text-center">
        <CLink onClick={() => document.body.scrollIntoView({ behavior:"smooth" })}>
          <CIcon name="cilArrowCircleTop" size="2xl" />
        </CLink>
      </div>
    </div>
  );
};

export default BooksIndexPage;