import moment from 'moment';
import { CBadge, CButton, CButtonGroup, CCard, CCardFooter, CCardHeader, CCol, CFormGroup, CInput, CLabel, CRow, CSelect, CTextarea } from '@coreui/react';
import React, { useCallback, useContext, useEffect, useMemo, useReducer, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';

import AuthContext from '../../context/AuthContext';

const URL_API_HOSTNAME    = window.location.hostname;
const URL_API_SINGLE_BOOK = `http://${URL_API_HOSTNAME}:8443/books`;

const BookType = {
  STANDARD: {
    value: 'book',
    label: 'Standard Book',
  },
  SOL_MAN: {
    value: 'solution-manual',
    label: 'Solution Manual',
  },
  TEST_BANK: {
    value: 'test-bank',
    label: 'Test Bank',
  },
};

const formDataInitialState = {
  identity: {
    title: null,
    type: BookType.STANDARD.value,
    isbn10: null,
    isbn13: null,
  },
  metadata: {
    category: null,
    author: null,
    description: null,
    edition: null,
    series: null,
    publisher: null,
    published_year: null,
    language: null,
    pages_count: null,
    price: null,
  },
  image_source_url: null,
  file_source_url: null,
};

const formDataReducer = (formData, action) => {
  switch (action.type) {
    case 'SET_TITLE':
      return { ...formData, identity: { ...formData.identity, title: action.payload || null } };
    case 'SET_BOOK_TYPE':
      return { ...formData, identity: { ...formData.identity, type: action.payload || null } };
    case 'SET_ISBN10':
      return { ...formData, identity: { ...formData.identity, isbn10: action.payload || null } };
    case 'SET_ISBN13':
      return { ...formData, identity: { ...formData.identity, isbn13: action.payload || null } };

    case 'SET_CATEGORY':
      return { ...formData, metadata: { ...formData.metadata, category: action.payload || null } };
    case 'SET_AUTHOR':
      return { ...formData, metadata: { ...formData.metadata, author: action.payload || null } };
    case 'SET_DESCRIPTION':
      return { ...formData, metadata: { ...formData.metadata, description: action.payload || null } };
    case 'SET_EDITION':
      return { ...formData, metadata: { ...formData.metadata, edition: action.payload || null } };
    case 'SET_SERIES':
      return { ...formData, metadata: { ...formData.metadata, series: action.payload || null } };
    case 'SET_PUBLISHER':
      return { ...formData, metadata: { ...formData.metadata, publisher: action.payload || null } };
    case 'SET_PUBLISHED_YEAR':
      return { ...formData, metadata: { ...formData.metadata, published_year: action.payload || null } };
    case 'SET_LANGUAGE':
      return { ...formData, metadata: { ...formData.metadata, language: action.payload || null } };
    case 'SET_PAGES_COUNT':
      return { ...formData, metadata: { ...formData.metadata, pages_count: action.payload || null } };
    case 'SET_PRICE':
      return { ...formData, metadata: { ...formData.metadata, price: action.payload || null } };

    case 'SET_IMAGE_SRC_URL':
      return { ...formData, image_source_url: action.payload || null };
    case 'SET_FILE_SRC_URL':
      return { ...formData, file_source_url: action.payload || null };
    case 'SET_FILE_UPLOAD':
      return { ...formData, file_upload: action.payload || null };

    case 'SET_ALL': {
      const { payload } = action;
      const { title, type, metadata, image, file } = payload;
      const isbnCouple = (BookType.STANDARD.value === type)
        ? { isbn10: payload.isbn10,         isbn13: payload.isbn13 }
        : { isbn10: payload.isbn10_related, isbn13: payload.isbn13_related };
      const formData = {
        identity: {
          ...formDataInitialState.identity,
          ...{ title, type, ...isbnCouple },
        },
        metadata: {
          ...formDataInitialState.metadata,
          ...metadata,
        },
        image_source_url: image.source_url || null,
        file_source_url : file.source_url || null,
      };
      return formData;
    }

    default:
      throw new Error(`Action type [${action.type}] not supported`);
  }
}

const btnSaveReducer = (btnSaveState, action) => {
  switch (action.type) {
    case 'ENABLE':
      return { disabled: false, label: 'Save' };
    case 'DISABLE':
      return { disabled: true, label: 'Saving...' };
    default:
      throw new Error(`Action type [${action.type} not supported]`)
  }
};

const ModifyBookPage = () => {
  const { id } = useParams();
  const history = useHistory();
  const authContext = useContext(AuthContext);
  const [bookDetails, setBookDetails] = useState(null);
  const [formData, dispatchFormData] = useReducer(formDataReducer, formDataInitialState);
  const [btnSaveState, dispatchBtnSaveState] = useReducer(btnSaveReducer, { disabled: false, label: 'Save' });
  const [respErrMsgs, setRespErrMsgs] = useState({});

  const enableBtnSubmit  = useCallback(() => dispatchBtnSaveState({ type: 'ENABLE' }), [dispatchBtnSaveState])
  const disableBtnSubmit = useCallback(() => dispatchBtnSaveState({ type: 'DISABLE' }), [dispatchBtnSaveState]);

  useEffect(() => {
    const url   = `${URL_API_SINGLE_BOOK}/${id}`;
    const token = authContext.jwtToken;
    fetch(url,
      { headers: { Authorization: `Bearer ${token}` }
    })
      .then(async resp => {
        const respOk  = resp.status < 400;
        const payload = await resp.json();
        if (respOk) {
          setBookDetails(payload.data);
          dispatchFormData({ type: 'SET_ALL', payload: payload.data });
        } else {
          if (resp.status === 400 && payload.errorMessages) {
            setRespErrMsgs(payload.errorMessages);
          } else if (resp.status === 401) {
            return authContext.emitAuthFailed();
          } else {
            throw new Error(payload.message || resp.statusText || 'Internal server error.');
          }
        }
      })
      .catch(err => {
        console.error(err);
        alert(err.message);
      })
      .finally(enableBtnSubmit);
  }, [id, enableBtnSubmit, authContext]);

  const handleFileChange = (event) => {
    this.setState({
      selectedFile: event.target.files[0]
    });
  };

  const errMsgsForIdentity = useMemo(() => {
    return Object.keys(respErrMsgs)
      .filter(field => field.startsWith('identity.'))
      .map(field => respErrMsgs[field][0]);
  }, [respErrMsgs]);

  const errMsgsForMetadata = useMemo(() => {
    return Object.keys(respErrMsgs)
      .filter(field => field.startsWith('metadata.'))
      .map(field => respErrMsgs[field][0]);
  }, [respErrMsgs]);

  const errMsgForImageSource = useMemo(() => {
    return respErrMsgs.image_source_url
      && respErrMsgs.image_source_url[0];
  }, [respErrMsgs]);

  const errMsgForFileSource = useMemo(() => {
    return respErrMsgs.file_source_url
      && respErrMsgs.file_source_url[0];
  }, [respErrMsgs]);

  const handleUpload = (event) => {
    // define upload
    event.preventDefault();
    const data = new FormData();
    data.append('file', this.state.selectedFile, this.state.selectedFile.name);
    axios.post('/upload', data, {
        onUploadProgress: (ProgressEvent) => {
          this.setState({
            loaded: (ProgressEvent.loaded / ProgressEvent.total) * 100
          });
        }
      })
      .then((res) => {
        console.log(res.statusText);
      });
  };

  const onSubmit = useCallback(async evt => {
    evt.preventDefault();
    handleUpload();
    disableBtnSubmit();
    try {
      const url   = `${URL_API_SINGLE_BOOK}/${id}`;
      const token = authContext.jwtToken;
      const resp = await fetch(url, {
        method: 'PUT',
        headers: {
          Authorization : `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const payload = await resp.json();
      const respOk  = resp.status < 400;

      if (respOk) {
        history.push('/books');
      } else {
        if (resp.status === 400 && payload.errorMessages) {
          setRespErrMsgs(payload.errorMessages);
        } else if (resp.status === 401) {
          return authContext.emitAuthFailed();
        } else {
          throw new Error(payload.message || resp.statusText || 'Internal server error.');
        }
      }
    } catch (err) {
      console.error(err);
      alert(err.message);
    } finally {
      enableBtnSubmit();
    }
  }, [id, formData, enableBtnSubmit, disableBtnSubmit, history, authContext]);

  return (
    <div className="content">
      <form onSubmit={onSubmit} enctype="multipart/form-data">
        <div className="card overflow-hidden">
          <CCardHeader>
            <CButton type="submit" color="primary" className="float-right" disabled={btnSaveState.disabled}>{btnSaveState.label}</CButton>
            <h3 className="card-title mb-0">
              Book Details:<br/>
              <small>{formData.identity.title} (#{id})</small>
            </h3>
          </CCardHeader>
          <div className="card-body mt-2">
            <h4 className="card-title">Book Identity</h4>
            { errMsgsForIdentity &&
              errMsgsForIdentity.length > 0 &&
              <CCard color='danger'>
                <CCardHeader>
                  { errMsgsForIdentity.map(
                    (value, index) => <div key={index} style={{ color: 'white' }}>{value}</div>
                  ) }
                </CCardHeader>
              </CCard>
            }
            <CFormGroup>
              <CLabel>Title</CLabel>
              <CInput type="text" name="identity[title]"
                value={formData.identity.title || ''}
                onChange={evt => dispatchFormData({ type: 'SET_TITLE', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Type</CLabel>
              <CSelect name="identity[type]"
                value={formData.identity.type}
                onChange={evt => dispatchFormData({ type: 'SET_BOOK_TYPE', payload: evt.currentTarget.value })}
              >
                {Object.values(BookType).map(
                  ({ label, value }) => <option key={value} value={value}>{label}</option>
                )}
              </CSelect>
            </CFormGroup>
            <CFormGroup>
              <CLabel>ISBN10</CLabel>
              <CInput type="text" name="identity[isbn10]"
                value={formData.identity.isbn10 || ''}
                onChange={evt => dispatchFormData({ type: 'SET_ISBN10', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>ISBN13</CLabel>
              <CInput type="text" name="identity[isbn13]"
                value={formData.identity.isbn13 || ''}
                onChange={evt => dispatchFormData({ type: 'SET_ISBN13', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
          </div>
          <div className="card-body border-top">
            <h4 className="card-title">Metadata</h4>
            { errMsgsForMetadata &&
              errMsgsForMetadata.length > 0 &&
              <CCard color='danger'>
                <CCardHeader>
                  { errMsgsForMetadata.map(
                    (value, index) => <div key={index} style={{ color: 'white' }}>{value}</div>
                  ) }
                </CCardHeader>
              </CCard>
            }
            <CFormGroup>
              <CLabel>Category</CLabel>
              <CInput type="text" name="metadata[category]"
                value={formData.metadata.category || ''}
                onChange={evt => dispatchFormData({ type: 'SET_CATEGORY', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Author</CLabel>
              <CInput type="text" name="metadata[author]"
                value={formData.metadata.author || ''}
                onChange={evt => dispatchFormData({ type: 'SET_AUTHOR', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Description</CLabel>
              <CTextarea name="metadata[description]" style={{ height: 256 }}
                value={formData.metadata.description || ''}
                onChange={evt => dispatchFormData({ type: 'SET_DESCRIPTION', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Edition</CLabel>
              <CInput type="text" name="metadata[edition]"
                value={formData.metadata.edition || ''}
                onChange={evt => dispatchFormData({ type: 'SET_EDITION', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Series</CLabel>
              <CInput type="text" name="metadata[series]"
                value={formData.metadata.series || ''}
                onChange={evt => dispatchFormData({ type: 'SET_SERIES', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Publisher</CLabel>
              <CInput type="text" name="metadata[publisher]"
                value={formData.metadata.publisher || ''}
                onChange={evt => dispatchFormData({ type: 'SET_PUBLISHER', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Published Year</CLabel>
              <CInput type="number" name="metadata[published_year]"
                value={formData.metadata.published_year || ''}
                onChange={evt => dispatchFormData({ type: 'SET_PUBLISHED_YEAR', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Language</CLabel>
              <CInput type="text" name="metadata[language]"
                value={formData.metadata.language || ''}
                onChange={evt => dispatchFormData({ type: 'SET_LANGUAGE', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Pages</CLabel>
              <CInput type="number" name="metadata[pages_count]"
                value={formData.metadata.pages_count || ''}
                onChange={evt => dispatchFormData({ type: 'SET_PAGES_COUNT', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>Price</CLabel>
              <CInput type="number" name="metadata[price]"
                value={formData.metadata.price || ''}
                onChange={evt => dispatchFormData({ type: 'SET_PRICE', payload: evt.currentTarget.value })}
              />
            </CFormGroup>
          </div>

          <div className="card-body border-top">
            <CRow>
              <CCol>
                <h4 className="card-title">Image</h4>
                { errMsgForImageSource &&
                  <CCard color='danger'>
                    <CCardHeader>
                      <div style={{ color: 'white' }}>{errMsgForImageSource}</div>
                    </CCardHeader>
                  </CCard>
                }
                <CFormGroup>
                  <CLabel>Source URL</CLabel>
                  <CInput type="text" name="image_source_url"
                    value={formData.image_source_url || ''}
                    onChange={evt => dispatchFormData({ type: 'SET_IMAGE_SRC_URL', payload: evt.currentTarget.value })}
                  />
                </CFormGroup>
                { bookDetails && bookDetails.image.b2_url &&
                  <div>
                    <a href={bookDetails.image.b2_url} target="_blank" rel="noopener noreferrer">B2 Resource URL</a>
                    <br />
                    <CBadge color="light">Uploaded at {moment(bookDetails.history.last_image_upload).format('DD/MM/YYYY HH:mm:ss')}</CBadge>
                  </div>
                }
              </CCol>
              <CCol>
                <h4 className="card-title">File</h4>
                { errMsgForFileSource &&
                  <CCard color='danger'>
                    <CCardHeader>
                      <div style={{ color: 'white' }}>{errMsgForFileSource}</div>
                    </CCardHeader>
                  </CCard>
                }
                <CFormGroup>
                  <CLabel>Source URL</CLabel>
                  <CInput type="text" name="file_source_url"
                    value={formData.file_source_url || ''}
                    onChange={evt => dispatchFormData({ type: 'SET_FILE_SRC_URL', payload: evt.currentTarget.value })}
                  />
                  <CInput type="file" name="file_upload"
                    onChange={handleFileChange}
                  />
                </CFormGroup>
                { bookDetails && bookDetails.file.b2_url &&
                  <div>
                    <a href={bookDetails.file.b2_url} target="_blank" rel="noopener noreferrer">B2 Resource URL</a>
                    <br />
                    <CBadge color="light">Uploaded at {moment(bookDetails.history.last_book_upload).format('DD/MM/YYYY HH:mm:ss')}</CBadge>
                  </div>
                }
              </CCol>
            </CRow>
          </div>

          <CCardFooter>
            <CButtonGroup className="float-right">
              <CButton type="submit" color="primary" disabled={btnSaveState.disabled}>{btnSaveState.label}</CButton>
            </CButtonGroup>
          </CCardFooter>
        </div>
      </form>
    </div>
  );
};

export default ModifyBookPage;
